export const creditentals = {
    clientId: 'CLIENT_ID',
    clientSecret: 'SECRET_ID',
    redirectUri: 'localhost:8081/auth'
}

// TO CREATE A TOKEN, PLEASE VISIT https://developer.spotify.com/console/get-playlist/
export const token: string = 'TOKEN'
