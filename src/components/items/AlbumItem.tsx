import React from 'react'
import { StyleSheet, TouchableOpacity, View, Image, Text, Dimensions } from 'react-native'
import colors from '../../constants/colors'

const deviceWidth = Dimensions.get("window").width
const itemWidth = 170
const padding = (deviceWidth - itemWidth * 2) / 3

interface Props {
    item: any;
    onPress: () => void
};

const AlbumItem: React.FC<Props> = props => {
    // console.log('item: ', props.item.track.artists[0].name)
    return (
        <TouchableOpacity style={styles.container} onPress={props.onPress}>
            <Image style={styles.image} source={{ uri: props.item.track?.album?.images[0].url }} />
            {/* <Text style={styles.title} >{props.item.track?.album?.name}</Text> */}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: itemWidth,
        height: itemWidth * 0.9,
        borderRadius: 8,
        marginBottom: 16,
        marginHorizontal: padding / 2
    },
    image: {
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 8
    },
    title: {
        color: 'white',
        position: 'absolute',
        bottom: 8,
        alignSelf: 'center',
        fontWeight:'bold',
        backgroundColor: colors.sgray
    }
})

export default AlbumItem