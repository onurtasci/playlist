import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import SpotifyWebApi from "spotify-web-api-node";
import Artist from '../../interfaces/Artist';
import Track from '../../interfaces/Track';
import { creditentals, token } from '../../env/api';

interface Props {
    track: Track;
};

const Songtrack: React.FC<Props> = props => {
    const [loading, setLoading] = useState<boolean>(false)
    const [artist, setArtist] = useState<Artist | null>(null)

    const spotifyApi = new SpotifyWebApi(creditentals);
    spotifyApi.setAccessToken(token);

    useEffect(() => {
        setLoading(true)
        spotifyApi.getArtist(props.track.artists[0].id).then(
          function (data: any) {
            const res = data.body
            setArtist(res)
            setLoading(false)
          },
          function (err: any) {
            console.error('ERR: ', err);
            setLoading(false)
          }
        );
      }, []) 
      
      if(loading) {
        return <View />
      }

    return (
        <View style={styles.container}>
            <Text style={styles.number} >{props.track.track_number.toString().length === 1 ? '0' + props.track.track_number.toString() : props.track.track_number}</Text>
            <Image style={styles.image} source={{uri: artist?.images[0].url}}/>
            <View style={styles.infoContainer}>
                <View >
                    <Text style={styles.title} numberOfLines={2} >{props.track.name}</Text>
                    <Text style={styles.info} numberOfLines={1} >{props.track.artists[0].name}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 8,
        borderRadius: 4,
        marginTop: 8,
    },
    number: {
        width: 32,
        color: 'white',
        fontSize: 15,
    },
    image: {
        width: 50,
        height: 50,
        resizeMode: 'cover',
    },
    infoContainer: {
        width: '55%',
        justifyContent: 'space-between',
        marginLeft: 16
    },
    title: {
        fontWeight: '500',
        fontSize: 14,
        color: 'white'
    },
    subTitle: {
        fontWeight: '400',
        fontSize: 12,
        color: '#AB2680'
    },
    info: {
        fontWeight: '400',
        fontSize: 12,
        color: '#999999'
    },
})

export default Songtrack