import React from 'react'
import { View, Image } from 'react-native'

export type Props = {
    name: string;
    size: number;
};

const CustomIcon: React.FC<Props> = (props) => {

    const width = props.size

    const iconObject = icons.find(ic => ic.name === props.name)

    return (
        <View>
            <Image
                style={{
                    width: width,
                    height: width,
                    resizeMode: 'cover',
                }}
                source={iconObject?.link}
            />
        </View>
    )
}

const icons = [
    { name: 'search', link: require('../../assets/icons/search.png')},
    { name: 'desc', link: require('../../assets/icons/desc.png')},

]

export default CustomIcon