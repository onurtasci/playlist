import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, Modal, TouchableOpacity, ScrollView } from 'react-native'
import colors from '../../constants/colors'
import ButtonIcon from './ButtonIcon'

interface Props {
    onPressFilter: (artist: string[], year: string[]) => void;
    artists: string[];
    years: string[];
};

const FilterModal: React.FC<Props> = props => {
    const [modalVisible, setModalVisible] = useState(false)
    const [selectedArtists, setSelectedArtists] = useState<string[]>([])
    const [selectedYears, setSelectedYears] = useState<string[]>([])

    const artistSelectionHandler = (name: string) => {
        let newList: string[] = [...selectedArtists]
        const exists = newList.find(c => c === name)
        if (exists) {
            newList = newList.filter(c => c !== name)
        } else {
            newList.push(name)
        }
        setSelectedArtists(newList)
    }

    const yearSelectionHandler = (name: string) => {
        let newList: string[] = [...selectedYears]
        const exists = newList.find(c => c === name)
        if (exists) {
            newList = newList.filter(c => c !== name)
        } else {
            newList.push(name)
        }
        setSelectedYears(newList)
    }

    return (
        <View>
            <ButtonIcon style={{ marginLeft: 16 }} name='filter' size={20} onPress={() => { setModalVisible(true) }} loading={false} />
            <Modal visible={modalVisible} animationType='fade' transparent={true} >
                <View style={styles.itemsContainer} >
                    <View style={styles.container} >
                        <ButtonIcon style={styles.closeButton} name='close' size={20} onPress={() => { setModalVisible(false) }} loading={false} />
                        <Text style={styles.title}>Select Artists</Text>
                        <ScrollView style={styles.scroll} contentContainerStyle={styles.grid} >
                            {props.artists.map(item => (
                                <TouchableOpacity
                                    key={item}
                                    style={selectedArtists.find(c => c === item) ? styles.filterItemSelected : styles.filterItem}
                                    onPress={artistSelectionHandler.bind(this, item)}
                                >
                                    <Text
                                        style={selectedArtists.find(c => c === item) ? styles.filterTextSelected : styles.filterText}
                                    >
                                        {item}</Text>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                        <Text style={styles.title}>Select Release Year</Text>
                        <ScrollView style={styles.scroll} contentContainerStyle={styles.grid} >
                            {props.years.map((item) => (
                                <TouchableOpacity
                                    key={item}
                                    style={selectedYears.find(c => c === item) ? styles.filterItemSelected : styles.filterItem}
                                    onPress={yearSelectionHandler.bind(this, item)}
                                >
                                    <Text
                                        style={selectedYears.find(c => c === item) ? styles.filterTextSelected : styles.filterText}
                                    >
                                        {item}
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>

                        <TouchableOpacity style={styles.button} onPress={() => { props.onPressFilter(selectedArtists, selectedYears); setModalVisible(false) }} >
                            <Text style={styles.buttonText} >Filtrar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    itemsContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        paddingHorizontal: 16,
        paddingVertical: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        width: '95%',
        height: '95%',
        paddingTop: 16,
        paddingBottom: 16,
        paddingHorizontal: 10,
        borderRadius: 4,

        backgroundColor: '#ccc',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5
    },
    closeButton: {
        position: 'absolute',
        top: 16,
        right: 8,
        paddingRight: 0
    },
    title: {
        fontWeight: 'bold',
        fontSize: 13,
        color: colors.primary,
        marginBottom: 8,
        textAlign: 'center',
        marginTop: 16
    },
    scroll: {
        borderColor: '#ccc',
        paddingVertical: 8,
        paddingHorizontal: 8,
        borderRadius: 8,
    },
    grid: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 37,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    filterItem: {
        height: 32,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.sgray,
        borderRadius: 16,
        marginRight: 12,
        paddingHorizontal: 16,
        marginBottom: 8
    },
    filterItemSelected: {
        height: 32,
        backgroundColor: colors.sgray,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.sgray,
        borderRadius: 16,
        marginRight: 12,
        paddingHorizontal: 16,
        marginBottom: 8
    },
    filterText: {
        fontSize: 12,
        fontWeight: '400',
        color: colors.sgray,
        textAlign: 'center'
    },
    filterTextSelected: {
        fontSize: 12,
        fontWeight: '400',
        color: '#FFFFFF',
    },
    button: {
        width: 91,
        height: 36,
        borderRadius: 18,
        borderWidth: 1,
        borderColor: colors.primary,
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 8
    },
    buttonText: {
        fontWeight: '500',
        fontSize: 16,
        color: colors.primary,
    },
})

export default FilterModal