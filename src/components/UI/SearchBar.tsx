import React, { useState, useEffect } from 'react'
import { StyleSheet, View, TextInput, Dimensions } from 'react-native'
import colors from '../../constants/colors'
import CustomIcon from './CustomIcon'

const deviceWidth = Dimensions.get('window').width

interface Props {
    data: any[]
    setFixedData: (data: any[]) => void
}

const SearchBar: React.FC<Props> = props => {
    const [input, setInput] = useState<string>()
    const [inMemoryData, setInMemoryData] = useState<any[]>([])

    useEffect(() => {
        setInMemoryData(props.data)
    }, [props.data])

    const searchHandler = (value: string) => {
        setInput(value)
        const filteredData = inMemoryData.filter(
            (item: any) => {
                let itemLowerCase1 = item.track?.album?.name.toLowerCase()
                console.log(itemLowerCase1)
                let itemLowerCase2 = item.track.artists[0].name.toLowerCase()

                let searchTermLowerCase = value.toLowerCase()

                return (
                    itemLowerCase1.indexOf(searchTermLowerCase) > -1
                    || itemLowerCase2.indexOf(searchTermLowerCase) > -1
                )
            }
        )
        props.setFixedData(filteredData)
    }

    return (
        <View style={styles.searchBar} >
            <TextInput style={styles.input}
                value={input}
                placeholderTextColor={colors.sgray}
                placeholder='Search by artists or album'
                onChangeText={(value) => searchHandler(value)}
            />
            <CustomIcon name='search' size={17.08} />
        </View>
    )
}

const styles = StyleSheet.create({
    searchBar: {
        width: deviceWidth - 68,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        backgroundColor: '#ccc',
        borderRadius: 8,
        paddingHorizontal: 10,
    },
    input: {
        flex: 1,
        fontWeight: '500',
        fontSize: 12,
        marginRight: 10,
        color: colors.sgray,
    }
})

export default SearchBar