import React from 'react'
import { TouchableOpacity, Image, View, ActivityIndicator } from 'react-native'
import colors from '../../constants/colors';

export type Props = {
    name: string;
    onPress: () => void
    style: object
    loading: boolean
    size: number
  };

const ButtonIcon: React.FC<Props> = (props) => {

    const width = props.size

    const iconObject = icons.find(ic => ic.name === props.name)

    if(props.loading) {
        <View style={{paddingHorizontal: 10, ...props.style}}>
            <ActivityIndicator size='small' color={colors.primary} />
        </View>
    }

    return (
        <TouchableOpacity style={{...props.style}} onPress={props.onPress} >
            <Image
                style={{
                    width: width,
                    height: width,
                    resizeMode: 'cover'
                }}
                // source={icons[props.name]}
                source={iconObject?.link}
            />
        </TouchableOpacity>
    )
}

const icons = [
    { name: 'logout', link: require('../../assets/icons/logout.png'), },
    { name: 'filter', link: require('../../assets/icons/filter.png'), },
    { name: 'close', link: require('../../assets/icons/close.png'), },
    { name: 'back', link: require('../../assets/icons/back.png'), },

]

export default ButtonIcon