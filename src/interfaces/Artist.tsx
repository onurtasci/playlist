interface Artist {
    external_urls: any[]
    href: string
    id: string
    name: string
    type: string
    uri: string
    images: any[]
}

export default Artist