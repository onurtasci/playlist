import Artist from "./Artist";

interface Track {
    artists: Artist[],
     available_markets: any[]
     disc_number: number
     duration_ms: number
     explicit: boolean
     external_urls :{
        spotify: string
     }
     href: string
     id: string
     is_local: boolean
     name:  string
     preview_url: string
     track_number: number
     type: string
     uri: string
     items: any[]
}

export default Track