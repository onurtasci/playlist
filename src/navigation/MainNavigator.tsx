import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import Colors from '../constants/colors';
import PlaylistScreen from '../screens/PlaylistScreen';
import AlbumScreen from '../screens/AlbumScreen';

const defaultNavOptions: any = {
    headerTitle: '',
    headerStyle: {
        backgroundColor: Colors.backgroundColor,
        elevation: 0,
        shadowRadius: 0,
        shadowOffset: {
            height: 0,
        },
    },
    headerBackTitleVisible: false,
    headerTintColor: 'white'
}

const ClientsStack = createStackNavigator()

const ClientsNavigator = () => {

    return (
        <ClientsStack.Navigator screenOptions={defaultNavOptions} >
            <ClientsStack.Screen
                name="Playlist"
                component={PlaylistScreen}
            />
            <ClientsStack.Screen
                name="Album"
                component={AlbumScreen}
            />
        </ClientsStack.Navigator>
    )
}

export default ClientsNavigator