export default {
    primary: '#B22EF6',
    secondary: '#FFC107',
    sgray: 'rgb(56,60,73)',
    gray: '#ccc',
    // backgroundColor: '#f8f8ff',
    backgroundColor: 'rgb(56,60,73)',
}
