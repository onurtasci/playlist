import React, { useState, useEffect } from 'react'
import { StyleSheet, ScrollView, Dimensions, ActivityIndicator, Image } from 'react-native'
import SpotifyWebApi from "spotify-web-api-node";
import { creditentals, token } from './../env/api';
import SongItem from '../components/items/SongItem';
import Track from '../interfaces/Track';

const deviceWidth = Dimensions.get("window").width
const itemWidth = 170
const padding = (deviceWidth - itemWidth * 2) / 3

interface Props {
  navigation: {
    navigate: (param: string, object: { person: object }) => void;
    setOptions: (params: object) => void;
    addListener: (param: string, func: () => void) => void;
  };
}

const AlbumScreen: React.FC<Props> = (props: any) => {
  const [loading, setLoading] = useState<boolean>(false)
  const [item, setItem] = useState<any>(null)
  const [tracks, setTracks] = useState<{items: Track[]} | null>(null)

  const id = props.route.params.id
  const title = props.route.params.title

  useEffect(() => {
    props.navigation.setOptions({
      headerTitle: title
    })
  }, [])

  const spotifyApi = new SpotifyWebApi(creditentals);
  spotifyApi.setAccessToken(token);

  useEffect(() => {
    setLoading(true)
    spotifyApi.getAlbum(id).then(
      function (data: any) {
        const res = data.body
        setItem(res)
        setTracks(res.tracks)
        console.log(res.tracks.href)
        setLoading(false)
      },
      function (err: any) {
        console.error('ERR: ', err);
        setLoading(false)
      }
    );
  }, []) 

  if (loading) {
    return <ActivityIndicator style={{ marginTop: 32 }} size='large' color='white' />
  }

  return (
    <ScrollView style={styles.screen} >
      <Image style={styles.image} source={{uri: item?.images[0].url}}/>
      {tracks?.items?.map((track: any) => (
        <SongItem key={track.id} track={track} />
      ))}
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    paddingHorizontal: 8
  },
  image: {
      width: 200,
      height: 200,
      resizeMode: 'cover',
      borderRadius: 100,
      alignSelf: 'center'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 42,
    paddingHorizontal: padding,
  },
})

export default AlbumScreen