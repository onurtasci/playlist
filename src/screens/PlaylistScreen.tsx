import React, { useState, useEffect } from 'react'
import { View, StyleSheet, FlatList, Dimensions, ActivityIndicator, StatusBar, Text } from 'react-native'
import SpotifyWebApi from "spotify-web-api-node";
import AlbumItem from '../components/items/AlbumItem';
import FilterModal from '../components/UI/FilterModal';
import SearchBar from '../components/UI/SearchBar';
import { creditentals, token } from '../env/api';

const deviceWidth = Dimensions.get("window").width
const itemWidth = 170
const padding = (deviceWidth - itemWidth * 2) / 3

interface Props {
  navigation: {
    navigate: (param: string, object: { person: object }) => void;
    setOptions: (params: object) => void;
    addListener: (param: string, func: () => void) => void;
  };
}

const PlaylistScreen: React.FC<Props> = (props: any) => {
  const [items, setItems] = useState<any[]>([])
  const [loading, setLoading] = useState<boolean>(false)
  const [searchedItems, setSearchedItems] = useState<any[]>([])
  const [artists, setArtists] = useState<string[]>([])
  const [years, setYears] = useState<string[]>([])
  const [error, setError] = useState<string>('')

  useEffect(() => {
    props.navigation.setOptions({
      header: () => (
        <View style={styles.header}>
          <SearchBar
            data={items}
            setFixedData={(val) => { setSearchedItems(val) }}
          />
          <FilterModal
            artists={artists}
            years={years}
            onPressFilter={filterHandler}
          />
        </View>
      ),
    })
  }, [artists, years])

  const filterHandler = (artists: string[], years: string[]) => {
    let filteredByArtist: any = []
    if (artists.length > 0) {
      for (const key in artists) {
        const singleList = items.filter(i => i.track.artists[0].name === artists[key])
        filteredByArtist = [...filteredByArtist, ...singleList]
      }
    } else {
      filteredByArtist = items
    }
    let filteredByYear: any = []
    if (years.length > 0) {
      for (const key in years) {
        const singleList = filteredByArtist.filter((i: any) => i.track.album.release_date.slice(0, 4) === years[key])
        filteredByYear = [...filteredByYear, ...singleList]
      }
    } else {
      filteredByYear = filteredByArtist
    }
    setSearchedItems(filteredByYear.filter((a: any, b: any) => filteredByYear.indexOf(a) === b))
  }

  const spotifyApi = new SpotifyWebApi(creditentals);
  spotifyApi.setAccessToken(token);

  useEffect(() => {
    setError('')
    setLoading(true)
    spotifyApi.getPlaylist('5ieJqeLJjjI8iJWaxeBLuK').then(
      (data: any) => {
        const list = data.body.tracks.items.reverse()
        setItems(list)
        setSearchedItems(list)

        const arts: string[] = []
        const years: string[] = []
        for (const key in list) {
          arts.push(list[key].track.artists[0].name)
          years.push(list[key].track.album.release_date.slice(0, 4))
        }
        setArtists(arts.filter((a, b) => arts.indexOf(a) === b))
        setYears(years.filter((a, b) => years.indexOf(a) === b))
        setLoading(false)
      }
    ).catch((err: any) => {
      console.log(err.message)
      if(err.message.includes('The access token expired')) {
        setError('The access token expired')
      } else {
        setError('To be able to run the app, please fill your credidentals on src/env/api.tsx file')
      }
      setLoading(false)

    })
  }, [])

  if (loading) {
    return <ActivityIndicator style={{ marginTop: 32 }} size='large' color='white' />
  }

  return (
    <View style={styles.screen} >
      <StatusBar animated={true} barStyle='light-content' />
      {searchedItems.length === 0 && <Text style={styles.noItem} >No items found</Text>}
      {error !== '' && <Text style={styles.noItem} >{error}</Text>}
      <FlatList
        numColumns={2}
        contentContainerStyle={styles.listContainer}
        style={styles.list}
        keyExtractor={(item, index) => index.toString()}
        data={searchedItems}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => (
          <AlbumItem item={item} onPress={() => props.navigation.navigate('Album', { id: item.track.album.id, title: item.track.album.name })} />
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    paddingTop: 16
  },
  list: {},
  listContainer: {
    justifyContent: 'space-between',
    alignSelf: 'center'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 42,
    paddingHorizontal: padding,
  },
  noItem: {
    color: 'white',
    textAlign: 'center',
    marginVertical: 16
  }
})

export default PlaylistScreen