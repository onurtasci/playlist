import React from 'react'
import { View, StyleSheet, Text } from 'react-native'


const InitialScreen: React.FC = () => {

  return (
    <View style={styles.screen} >
        <Text style={styles.info}>To be able to run the app, please fill your credidentals on src/env/api.tsx file</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  info: {
    color: 'white',
  }
})

export default InitialScreen